
# IDMxS Lens Designs for Mobile Cameras

Source content is found under [`/content/`](/content/).

Pages are deployed to [https://idmxs.gitlab.io/lens-designs/](https://idmxs.gitlab.io/lens-designs/).

