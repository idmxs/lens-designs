---
title: Lens design 011 US20130342919A1
lens:
  focal: 3.760000
  fnum: 2.430000
  field: 36.100000
  wavelength: 0.587600
---

# Lens design 011: US20130342919A1

![layout](US20130342919A1.jpg)