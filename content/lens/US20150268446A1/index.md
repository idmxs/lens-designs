---
title: Lens design 017 US20150268446A1
lens:
  focal: 1.900000
  fnum: 2.600000
  field: 63.500000
  wavelength: 0.587600
---

# Lens design 017: US20150268446A1

![layout](US20150268446A1.jpg)