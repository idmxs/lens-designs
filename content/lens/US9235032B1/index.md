---
title: Lens design 081 US9235032B1
lens:
  focal: 4.110000
  fnum: 2.230000
  field: 36.000000
  wavelength: 0.587600
---

# Lens design 081: US9235032B1

![layout](US9235032B1.jpg)