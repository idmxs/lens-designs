---
title: Lens design 095 US9557534B1
lens:
  focal: 3.860000
  fnum: 2.440000
  field: 60.000000
  wavelength: 0.587600
---

# Lens design 095: US9557534B1

![layout](US9557534B1.jpg)