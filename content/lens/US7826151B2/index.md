---
title: Lens design 034 US7826151B2
lens:
  focal: 3.820000
  fnum: 2.800000
  field: 32.600000
  wavelength: 0.587600
---

# Lens design 034: US7826151B2

![layout](US7826151B2.jpg)