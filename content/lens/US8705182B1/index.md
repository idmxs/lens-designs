---
title: Lens design 063 US8705182B1
lens:
  focal: 3.790000
  fnum: 2.230000
  field: 37.300000
  wavelength: 0.587600
---

# Lens design 063: US8705182B1

![layout](US8705182B1.jpg)