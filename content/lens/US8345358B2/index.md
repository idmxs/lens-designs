---
title: Lens design 045 US8345358B2
lens:
  focal: 5.000000
  fnum: 2.450000
  field: 38.100000
  wavelength: 0.587600
---

# Lens design 045: US8345358B2

![layout](US8345358B2.jpg)