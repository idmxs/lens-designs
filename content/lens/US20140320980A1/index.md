---
title: Lens design 016 US20140320980A1
lens:
  focal: 4.240000
  fnum: 1.870000
  field: 36.400000
  wavelength: 0.587600
---

# Lens design 016: US20140320980A1

![layout](US20140320980A1.jpg)