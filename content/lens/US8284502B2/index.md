---
title: Lens design 042 US8284502B2
lens:
  focal: 5.990000
  fnum: 2.800000
  field: 31.000000
  wavelength: 0.587600
---

# Lens design 042: US8284502B2

![layout](US8284502B2.jpg)