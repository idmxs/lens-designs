---
title: Lens design 020 US20160048004A1
lens:
  focal: 3.940000
  fnum: 2.130000
  field: 40.300000
  wavelength: 0.587600
---

# Lens design 020: US20160048004A1

![layout](US20160048004A1.jpg)