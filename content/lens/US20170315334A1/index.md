---
title: Lens design 027 US20170315334A1
lens:
  focal: 5.710000
  fnum: 4.480000
  field: 21.500000
  wavelength: 0.587600
---

# Lens design 027: US20170315334A1

![layout](US20170315334A1.jpg)