---
title: Lens design 018 US20150338607A1
lens:
  focal: 4.160000
  fnum: 1.600000
  field: 36.000000
  wavelength: 0.587600
---

# Lens design 018: US20150338607A1

![layout](US20150338607A1.jpg)