---
title: Lens design 088 US9411133B1
lens:
  focal: 5.220000
  fnum: 2.400000
  field: 20.800000
  wavelength: 0.587600
---

# Lens design 088: US9411133B1

![layout](US9411133B1.jpg)