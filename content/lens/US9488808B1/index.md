---
title: Lens design 090 US9488808B1
lens:
  focal: 6.180000
  fnum: 3.000000
  field: 24.800000
  wavelength: 0.587600
---

# Lens design 090: US9488808B1

![layout](US9488808B1.jpg)