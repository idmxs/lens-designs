---
title: Lens design 048 US8379324B2
lens:
  focal: 4.170000
  fnum: 2.440000
  field: 34.600000
  wavelength: 0.587600
---

# Lens design 048: US8379324B2

![layout](US8379324B2.jpg)