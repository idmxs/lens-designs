---
title: Lens design 051 US8456758B1
lens:
  focal: 3.530000
  fnum: 2.750000
  field: 38.600000
  wavelength: 0.587600
---

# Lens design 051: US8456758B1

![layout](US8456758B1.jpg)