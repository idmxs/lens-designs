---
title: Lens design 064 US8736979B2
lens:
  focal: 3.240000
  fnum: 2.200000
  field: 42.400000
  wavelength: 0.587600
---

# Lens design 064: US8736979B2

![layout](US8736979B2.jpg)