---
title: Lens design 012 US20140111876A1
lens:
  focal: 4.200000
  fnum: 2.070000
  field: 37.600000
  wavelength: 0.587600
---

# Lens design 012: US20140111876A1

![layout](US20140111876A1.jpg)