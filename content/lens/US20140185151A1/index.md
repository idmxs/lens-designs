---
title: Lens design 015 US20140185151A1
lens:
  focal: 1.360000
  fnum: 2.050000
  field: 37.400000
  wavelength: 0.587600
---

# Lens design 015: US20140185151A1

![layout](US20140185151A1.jpg)