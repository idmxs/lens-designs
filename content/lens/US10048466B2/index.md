---
title: Lens design 003 US10048466B2
lens:
  focal: 2.300000
  fnum: 2.800000
  field: 67.000000
  wavelength: 0.587600
---

# Lens design 003: US10048466B2

![layout](US10048466B2.jpg)