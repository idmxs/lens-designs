---
title: Lens design 092 US9507126B2
lens:
  focal: 4.120000
  fnum: 1.430000
  field: 34.800000
  wavelength: 0.587600
---

# Lens design 092: US9507126B2

![layout](US9507126B2.jpg)