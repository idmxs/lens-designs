---
title: Lens design 036 US7869142B2
lens:
  focal: 4.240000
  fnum: 2.850000
  field: 33.700000
  wavelength: 0.587600
---

# Lens design 036: US7869142B2

![layout](US7869142B2.jpg)