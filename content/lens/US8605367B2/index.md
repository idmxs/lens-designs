---
title: Lens design 060 US8605367B2
lens:
  focal: 1.760000
  fnum: 2.600000
  field: 35.900000
  wavelength: 0.587600
---

# Lens design 060: US8605367B2

![layout](US8605367B2.jpg)