---
title: Lens design 050 US8441745B2
lens:
  focal: 1.280000
  fnum: 2.350000
  field: 45.100000
  wavelength: 0.587600
---

# Lens design 050: US8441745B2

![layout](US8441745B2.jpg)