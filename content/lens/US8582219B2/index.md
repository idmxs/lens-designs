---
title: Lens design 059 US8582219B2
lens:
  focal: 3.870000
  fnum: 2.070000
  field: 31.400000
  wavelength: 0.587600
---

# Lens design 059: US8582219B2

![layout](US8582219B2.jpg)