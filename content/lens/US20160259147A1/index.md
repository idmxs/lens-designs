---
title: Lens design 022 US20160259147A1
lens:
  focal: 3.880000
  fnum: 2.050000
  field: 37.000000
  wavelength: 0.587600
---

# Lens design 022: US20160259147A1

![layout](US20160259147A1.jpg)