---
title: Lens design 032 US7468847B2
lens:
  focal: 2.820000
  fnum: 2.450000
  field: 32.600000
  wavelength: 0.587600
---

# Lens design 032: US7468847B2

![layout](US7468847B2.jpg)