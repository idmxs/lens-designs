---
title: Lens design 009 US20100315723A1
lens:
  focal: 3.630000
  fnum: 2.800000
  field: 40.300000
  wavelength: 0.587600
---

# Lens design 009: US20100315723A1

![layout](US20100315723A1.jpg)