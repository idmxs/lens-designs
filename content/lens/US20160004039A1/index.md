---
title: Lens design 019 US20160004039A1
lens:
  focal: 3.780000
  fnum: 2.450000
  field: 38.700000
  wavelength: 0.587600
---

# Lens design 019: US20160004039A1

![layout](US20160004039A1.jpg)