---
title: Lens design 005 US10310223B2
lens:
  focal: 8.630000
  fnum: 2.060000
  field: 18.000000
  wavelength: 0.587600
---

# Lens design 005: US10310223B2

![layout](US10310223B2.jpg)