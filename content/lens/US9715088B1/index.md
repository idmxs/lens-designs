---
title: Lens design 098 US9715088B1
lens:
  focal: 2.650000
  fnum: 2.080000
  field: 80.700000
  wavelength: 0.587600
---

# Lens design 098: US9715088B1

![layout](US9715088B1.jpg)