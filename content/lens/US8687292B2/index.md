---
title: Lens design 062 US8687292B2
lens:
  focal: 3.200000
  fnum: 2.100000
  field: 41.200000
  wavelength: 0.587600
---

# Lens design 062: US8687292B2

![layout](US8687292B2.jpg)