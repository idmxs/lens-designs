---
title: Lens design 001 US10007086B2
lens:
  focal: 3.790000
  fnum: 1.650000
  field: 48.000000
  wavelength: 0.587600
---

# Lens design 001: US10007086B2

![layout](US10007086B2.jpg)