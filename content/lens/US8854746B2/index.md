---
title: Lens design 066 US8854746B2
lens:
  focal: 3.750000
  fnum: 2.080000
  field: 38.900000
  wavelength: 0.587600
---

# Lens design 066: US8854746B2

![layout](US8854746B2.jpg)