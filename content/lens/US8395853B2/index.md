---
title: Lens design 049 US8395853B2
lens:
  focal: 2.950000
  fnum: 3.200000
  field: 37.700000
  wavelength: 0.587600
---

# Lens design 049: US8395853B2

![layout](US8395853B2.jpg)