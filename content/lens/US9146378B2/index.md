---
title: Lens design 075 US9146378B2
lens:
  focal: 1.160000
  fnum: 2.000000
  field: 41.000000
  wavelength: 0.587600
---

# Lens design 075: US9146378B2

![layout](US9146378B2.jpg)