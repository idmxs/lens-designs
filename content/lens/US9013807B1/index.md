---
title: Lens design 071 US9013807B1
lens:
  focal: 6.190000
  fnum: 2.200000
  field: 38.500000
  wavelength: 0.587600
---

# Lens design 071: US9013807B1

![layout](US9013807B1.jpg)