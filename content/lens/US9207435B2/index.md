---
title: Lens design 080 US9207435B2
lens:
  focal: 4.380000
  fnum: 2.200000
  field: 38.200000
  wavelength: 0.587600
---

# Lens design 080: US9207435B2

![layout](US9207435B2.jpg)