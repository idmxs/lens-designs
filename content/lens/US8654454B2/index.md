---
title: Lens design 061 US8654454B2
lens:
  focal: 1.420000
  fnum: 2.850000
  field: 31.600000
  wavelength: 0.587600
---

# Lens design 061: US8654454B2

![layout](US8654454B2.jpg)