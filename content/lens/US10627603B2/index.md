---
title: Lens design 008 US10627603B2
lens:
  focal: 7.290000
  fnum: 2.250000
  field: 21.600000
  wavelength: 0.587600
---

# Lens design 008: US10627603B2

![layout](US10627603B2.jpg)