---
title: Lens design 099 US9753249B2
lens:
  focal: 8.590000
  fnum: 1.650000
  field: 19.800000
  wavelength: 0.587600
---

# Lens design 099: US9753249B2

![layout](US9753249B2.jpg)