---
title: Lens design 082 US9239447B1
lens:
  focal: 3.030000
  fnum: 2.200000
  field: 45.000000
  wavelength: 0.587600
---

# Lens design 082: US9239447B1

![layout](US9239447B1.jpg)