---
title: Lens design 065 US8743478B2
lens:
  focal: 4.050000
  fnum: 2.600000
  field: 34.300000
  wavelength: 0.587600
---

# Lens design 065: US8743478B2

![layout](US8743478B2.jpg)