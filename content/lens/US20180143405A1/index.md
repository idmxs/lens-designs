---
title: Lens design 029 US20180143405A1
lens:
  focal: 3.070000
  fnum: 1.860000
  field: 47.500000
  wavelength: 0.587600
---

# Lens design 029: US20180143405A1

![layout](US20180143405A1.jpg)