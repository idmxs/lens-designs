---
title: Lens design 047 US8358473B2
lens:
  focal: 3.130000
  fnum: 2.920000
  field: 36.100000
  wavelength: 0.587600
---

# Lens design 047: US8358473B2

![layout](US8358473B2.jpg)