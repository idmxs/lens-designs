---
title: Lens design 076 US9151931B1
lens:
  focal: 4.170000
  fnum: 2.450000
  field: 34.300000
  wavelength: 0.587600
---

# Lens design 076: US9151931B1

![layout](US9151931B1.jpg)