---
title: Lens design 068 US8908290B1
lens:
  focal: 3.850000
  fnum: 2.260000
  field: 45.600000
  wavelength: 0.587600
---

# Lens design 068: US8908290B1

![layout](US8908290B1.jpg)