---
title: Lens design 101 US9857559B2
lens:
  focal: 6.070000
  fnum: 2.400000
  field: 22.000000
  wavelength: 0.587600
---

# Lens design 101: US9857559B2

![layout](US9857559B2.jpg)