---
title: Lens design 014 US20140126073A1
lens:
  focal: 3.540000
  fnum: 2.040000
  field: 39.000000
  wavelength: 0.587600
---

# Lens design 014: US20140126073A1

![layout](US20140126073A1.jpg)