---
title: Lens design 070 US8988796B1
lens:
  focal: 1.660000
  fnum: 2.150000
  field: 46.800000
  wavelength: 0.587600
---

# Lens design 070: US8988796B1

![layout](US8988796B1.jpg)