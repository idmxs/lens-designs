---
title: Lens design 058 US8531786B2
lens:
  focal: 3.290000
  fnum: 2.400000
  field: 34.100000
  wavelength: 0.587600
---

# Lens design 058: US8531786B2

![layout](US8531786B2.jpg)