---
title: Lens design 052 US8462449B2
lens:
  focal: 4.390000
  fnum: 2.950000
  field: 37.400000
  wavelength: 0.587600
---

# Lens design 052: US8462449B2

![layout](US8462449B2.jpg)