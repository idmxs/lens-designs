---
title: Lens design 030 US20180348484A1
lens:
  focal: 3.930000
  fnum: 1.450000
  field: 39.000000
  wavelength: 0.587600
---

# Lens design 030: US20180348484A1

![layout](US20180348484A1.jpg)