---
title: Lens design 089 US9417438B2
lens:
  focal: 1.580000
  fnum: 2.350000
  field: 48.000000
  wavelength: 0.587600
---

# Lens design 089: US9417438B2

![layout](US9417438B2.jpg)