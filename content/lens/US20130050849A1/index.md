---
title: Lens design 010 US20130050849A1
lens:
  focal: 1.660000
  fnum: 2.450000
  field: 31.400000
  wavelength: 0.587600
---

# Lens design 010: US20130050849A1

![layout](US20130050849A1.jpg)