---
title: Lens design 072 US9063319B1
lens:
  focal: 4.700000
  fnum: 2.250000
  field: 39.000000
  wavelength: 0.587600
---

# Lens design 072: US9063319B1

![layout](US9063319B1.jpg)