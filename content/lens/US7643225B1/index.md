---
title: Lens design 033 US7643225B1
lens:
  focal: 3.340000
  fnum: 2.450000
  field: 35.800000
  wavelength: 0.587600
---

# Lens design 033: US7643225B1

![layout](US7643225B1.jpg)