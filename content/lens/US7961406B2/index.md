---
title: Lens design 038 US7961406B2
lens:
  focal: 3.350000
  fnum: 2.900000
  field: 33.800000
  wavelength: 0.587600
---

# Lens design 038: US7961406B2

![layout](US7961406B2.jpg)