---
title: Lens design 040 US8179614B1
lens:
  focal: 3.860000
  fnum: 2.400000
  field: 36.300000
  wavelength: 0.587600
---

# Lens design 040: US8179614B1

![layout](US8179614B1.jpg)