---
title: Lens design 023 US20170003482A1
lens:
  focal: 3.620000
  fnum: 2.320000
  field: 45.500000
  wavelength: 0.587600
---

# Lens design 023: US20170003482A1

![layout](US20170003482A1.jpg)