---
title: Lens design 091 US9507124B2
lens:
  focal: 1.550000
  fnum: 2.650000
  field: 39.200000
  wavelength: 0.587600
---

# Lens design 091: US9507124B2

![layout](US9507124B2.jpg)