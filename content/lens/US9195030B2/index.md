---
title: Lens design 078 US9195030B2
lens:
  focal: 4.210000
  fnum: 2.240000
  field: 38.700000
  wavelength: 0.587600
---

# Lens design 078: US9195030B2

![layout](US9195030B2.jpg)