---
title: Lens design 028 US20180031807A1
lens:
  focal: 2.740000
  fnum: 2.800000
  field: 79.900000
  wavelength: 0.587600
---

# Lens design 028: US20180031807A1

![layout](US20180031807A1.jpg)