---
title: Lens design 044 US8335043B2
lens:
  focal: 5.430000
  fnum: 2.830000
  field: 39.600000
  wavelength: 0.587600
---

# Lens design 044: US8335043B2

![layout](US8335043B2.jpg)