---
title: Lens design 087 US9348117B1
lens:
  focal: 3.760000
  fnum: 2.050000
  field: 37.500000
  wavelength: 0.587600
---

# Lens design 087: US9348117B1

![layout](US9348117B1.jpg)