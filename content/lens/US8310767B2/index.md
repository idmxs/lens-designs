---
title: Lens design 043 US8310767B2
lens:
  focal: 2.990000
  fnum: 2.900000
  field: 37.200000
  wavelength: 0.587600
---

# Lens design 043: US8310767B2

![layout](US8310767B2.jpg)