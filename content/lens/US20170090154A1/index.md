---
title: Lens design 025 US20170090154A1
lens:
  focal: 3.510000
  fnum: 2.400000
  field: 19.000000
  wavelength: 0.587600
---

# Lens design 025: US20170090154A1

![layout](US20170090154A1.jpg)