---
title: Lens design 035 US7864454B1
lens:
  focal: 5.330000
  fnum: 2.900000
  field: 33.500000
  wavelength: 0.587600
---

# Lens design 035: US7864454B1

![layout](US7864454B1.jpg)