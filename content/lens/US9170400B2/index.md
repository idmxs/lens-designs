---
title: Lens design 077 US9170400B2
lens:
  focal: 5.650000
  fnum: 2.300000
  field: 35.500000
  wavelength: 0.587600
---

# Lens design 077: US9170400B2

![layout](US9170400B2.jpg)