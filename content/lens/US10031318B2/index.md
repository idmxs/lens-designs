---
title: Lens design 002 US10031318B2
lens:
  focal: 4.780000
  fnum: 2.050000
  field: 39.200000
  wavelength: 0.587600
---

# Lens design 002: US10031318B2

![layout](US10031318B2.jpg)