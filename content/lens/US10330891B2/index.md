---
title: Lens design 007 US10330891B2
lens:
  focal: 2.420000
  fnum: 2.080000
  field: 50.000000
  wavelength: 0.587600
---

# Lens design 007: US10330891B2

![layout](US10330891B2.jpg)