---
title: Lens design 039 US8077400B2
lens:
  focal: 1.580000
  fnum: 2.400000
  field: 30.000000
  wavelength: 0.587600
---

# Lens design 039: US8077400B2

![layout](US8077400B2.jpg)