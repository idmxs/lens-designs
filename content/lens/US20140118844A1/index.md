---
title: Lens design 013 US20140118844A1
lens:
  focal: 3.480000
  fnum: 2.400000
  field: 39.600000
  wavelength: 0.587600
---

# Lens design 013: US20140118844A1

![layout](US20140118844A1.jpg)