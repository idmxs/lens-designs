---
title: Lens design 031 US20190041610A1
lens:
  focal: 4.580000
  fnum: 2.450000
  field: 40.000000
  wavelength: 0.587600
---

# Lens design 031: US20190041610A1

![layout](US20190041610A1.jpg)