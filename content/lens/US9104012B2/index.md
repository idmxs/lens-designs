---
title: Lens design 073 US9104012B2
lens:
  focal: 2.870000
  fnum: 2.200000
  field: 37.600000
  wavelength: 0.587600
---

# Lens design 073: US9104012B2

![layout](US9104012B2.jpg)