---
title: Lens design 093 US9557530B2
lens:
  focal: 3.300000
  fnum: 2.250000
  field: 40.500000
  wavelength: 0.587600
---

# Lens design 093: US9557530B2

![layout](US9557530B2.jpg)