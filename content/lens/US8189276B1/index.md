---
title: Lens design 041 US8189276B1
lens:
  focal: 1.050000
  fnum: 2.600000
  field: 68.000000
  wavelength: 0.587600
---

# Lens design 041: US8189276B1

![layout](US8189276B1.jpg)