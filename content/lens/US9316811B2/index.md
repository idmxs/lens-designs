---
title: Lens design 085 US9316811B2
lens:
  focal: 4.830000
  fnum: 2.200000
  field: 38.100000
  wavelength: 0.587600
---

# Lens design 085: US9316811B2

![layout](US9316811B2.jpg)