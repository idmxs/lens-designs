---
title: Lens design 067 US8896936B2
lens:
  focal: 3.460000
  fnum: 2.550000
  field: 32.100000
  wavelength: 0.587600
---

# Lens design 067: US8896936B2

![layout](US8896936B2.jpg)