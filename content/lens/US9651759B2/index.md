---
title: Lens design 097 US9651759B2
lens:
  focal: 3.270000
  fnum: 2.200000
  field: 41.000000
  wavelength: 0.587600
---

# Lens design 097: US9651759B2

![layout](US9651759B2.jpg)