---
title: Lens design 037 US7916401B2
lens:
  focal: 2.980000
  fnum: 2.400000
  field: 31.900000
  wavelength: 0.587600
---

# Lens design 037: US7916401B2

![layout](US7916401B2.jpg)