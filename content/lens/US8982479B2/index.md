---
title: Lens design 069 US8982479B2
lens:
  focal: 1.610000
  fnum: 2.840000
  field: 75.000000
  wavelength: 0.587600
---

# Lens design 069: US8982479B2

![layout](US8982479B2.jpg)