---
title: Lens design 084 US9316809B2
lens:
  focal: 1.970000
  fnum: 2.060000
  field: 37.500000
  wavelength: 0.587600
---

# Lens design 084: US9316809B2

![layout](US9316809B2.jpg)