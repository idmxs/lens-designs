---
title: Lens design 056 US8488259B2
lens:
  focal: 4.320000
  fnum: 2.450000
  field: 32.900000
  wavelength: 0.587600
---

# Lens design 056: US8488259B2

![layout](US8488259B2.jpg)