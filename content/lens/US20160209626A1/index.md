---
title: Lens design 021 US20160209626A1
lens:
  focal: 2.400000
  fnum: 2.500000
  field: 49.800000
  wavelength: 0.587600
---

# Lens design 021: US20160209626A1

![layout](US20160209626A1.jpg)