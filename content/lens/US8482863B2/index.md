---
title: Lens design 054 US8482863B2
lens:
  focal: 3.770000
  fnum: 2.400000
  field: 36.700000
  wavelength: 0.587600
---

# Lens design 054: US8482863B2

![layout](US8482863B2.jpg)