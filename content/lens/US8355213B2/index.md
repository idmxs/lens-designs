---
title: Lens design 046 US8355213B2
lens:
  focal: 3.230000
  fnum: 2.880000
  field: 34.900000
  wavelength: 0.587600
---

# Lens design 046: US8355213B2

![layout](US8355213B2.jpg)