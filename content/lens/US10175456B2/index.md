---
title: Lens design 004 US10175456B2
lens:
  focal: 5.050000
  fnum: 2.480000
  field: 27.000000
  wavelength: 0.587600
---

# Lens design 004: US10175456B2

![layout](US10175456B2.jpg)