---
title: Lens design 103 US9961244B2
lens:
  focal: 7.760000
  fnum: 1.600000
  field: 21.000000
  wavelength: 0.587600
---

# Lens design 103: US9961244B2

![layout](US9961244B2.jpg)