---
title: Lens design 006 US10324272B2
lens:
  focal: 5.750000
  fnum: 2.350000
  field: 22.300000
  wavelength: 0.587600
---

# Lens design 006: US10324272B2

![layout](US10324272B2.jpg)