---
title: Lens design 086 US9348115B1
lens:
  focal: 1.280000
  fnum: 2.070000
  field: 100.000000
  wavelength: 0.587600
---

# Lens design 086: US9348115B1

![layout](US9348115B1.jpg)