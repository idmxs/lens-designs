---
title: Lens design 102 US9958645B2
lens:
  focal: 4.460000
  fnum: 2.250000
  field: 40.500000
  wavelength: 0.587600
---

# Lens design 102: US9958645B2

![layout](US9958645B2.jpg)