---
title: Lens design 053 US8482863B2-table-4
lens:
  focal: 3.480000
  fnum: 2.400000
  field: 38.900000
  wavelength: 0.587600
---

# Lens design 053: US8482863B2-table-4

![layout](US8482863B2-table-4.jpg)