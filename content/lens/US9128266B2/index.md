---
title: Lens design 074 US9128266B2
lens:
  focal: 2.270000
  fnum: 2.020000
  field: 44.500000
  wavelength: 0.587600
---

# Lens design 074: US9128266B2

![layout](US9128266B2.jpg)