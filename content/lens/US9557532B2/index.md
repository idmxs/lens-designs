---
title: Lens design 094 US9557532B2
lens:
  focal: 4.140000
  fnum: 2.300000
  field: 35.000000
  wavelength: 0.587600
---

# Lens design 094: US9557532B2

![layout](US9557532B2.jpg)