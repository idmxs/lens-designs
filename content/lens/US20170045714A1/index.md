---
title: Lens design 024 US20170045714A1
lens:
  focal: 3.970000
  fnum: 1.750000
  field: 35.200000
  wavelength: 0.587600
---

# Lens design 024: US20170045714A1

![layout](US20170045714A1.jpg)