---
title: Lens design 055 US8488255B2
lens:
  focal: 3.630000
  fnum: 2.500000
  field: 37.700000
  wavelength: 0.587600
---

# Lens design 055: US8488255B2

![layout](US8488255B2.jpg)