---
title: Lens design 079 US9201216B2
lens:
  focal: 3.270000
  fnum: 2.100000
  field: 42.000000
  wavelength: 0.587600
---

# Lens design 079: US9201216B2

![layout](US9201216B2.jpg)