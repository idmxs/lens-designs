---
title: Lens design 057 US8508859B2
lens:
  focal: 4.380000
  fnum: 2.400000
  field: 32.700000
  wavelength: 0.587600
---

# Lens design 057: US8508859B2

![layout](US8508859B2.jpg)