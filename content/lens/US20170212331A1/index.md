---
title: Lens design 026 US20170212331A1
lens:
  focal: 1.900000
  fnum: 2.700000
  field: 68.000000
  wavelength: 0.587600
---

# Lens design 026: US20170212331A1

![layout](US20170212331A1.jpg)