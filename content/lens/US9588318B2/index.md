---
title: Lens design 096 US9588318B2
lens:
  focal: 4.630000
  fnum: 2.050000
  field: 39.900000
  wavelength: 0.587600
---

# Lens design 096: US9588318B2

![layout](US9588318B2.jpg)