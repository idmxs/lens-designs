---
title: Lens design 100 US9810880B2
lens:
  focal: 4.990000
  fnum: 2.250000
  field: 41.700000
  wavelength: 0.587600
---

# Lens design 100: US9810880B2

![layout](US9810880B2.jpg)