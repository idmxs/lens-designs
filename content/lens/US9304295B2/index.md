---
title: Lens design 083 US9304295B2
lens:
  focal: 3.560000
  fnum: 2.500000
  field: 39.300000
  wavelength: 0.587600
---

# Lens design 083: US9304295B2

![layout](US9304295B2.jpg)